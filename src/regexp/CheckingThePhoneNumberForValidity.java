package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckingThePhoneNumberForValidity {
    public static void main(String[] args) {

        String phoneNumber = "+380442283228";
        Pattern pattern = Pattern.compile("^((\\+?380)([0-9]{9}))$");
        Matcher matcher = pattern.matcher(phoneNumber);

        if (matcher.matches()){
            System.out.println("Номер телефона " + phoneNumber + " валидный"); // валидный
        } else {
            System.out.println("Номер телефона " + phoneNumber + " невалидный");
        }

        String phoneNumber1 = "380442283228";
        Pattern pattern1 = Pattern.compile("^((\\+?380)(\\d{9}))$");
        Matcher matcher1 = pattern1.matcher(phoneNumber1);

        if (matcher1.matches()){
            System.out.println("Номер телефона " + phoneNumber1 + " валидный"); // валидный
        } else {
            System.out.println("Номер телефона " + phoneNumber1 + " невалидный");
        }

        String phoneNumber2 = "+3804422083228";
        Pattern pattern2 = Pattern.compile("^((\\+?380)(\\p{Digit}{9}))$");
        Matcher matcher2 = pattern2.matcher(phoneNumber2);

        if (matcher2.matches()){
            System.out.println("Номер телефона " + phoneNumber2 + " валидный");
        } else {
            System.out.println("Номер телефона " + phoneNumber2 + " невалидный"); // невалидный
        }
    }
}
