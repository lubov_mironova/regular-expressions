package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpGrouping {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(\\d+)");
        Matcher matcher = pattern.matcher("2016 year. 2017 year. 2018 year");

        while (matcher.find()) {
            System.out.println(matcher.group(1)); // 2016 2017 2018
        }

        Pattern pattern1 = Pattern.compile("(\\d+).*\\1");
        Matcher matcher1 = pattern1.matcher("2016 year. 2017 year. 2018 year");

        while (matcher1.find()) {
            System.out.println(matcher1.group(1)); // 201
        }

        Pattern pattern2 = Pattern.compile("(?:Mouse|Keyboard)Listener");
        Matcher matcher2 = pattern2.matcher("MouseListener KeyboardListener");

        while (matcher2.find()) {
            System.out.println(matcher2.group()); //MouseListener KeyboardListener
        }

        Pattern pattern3 = Pattern.compile("Java (?=7|8)");
        Matcher matcher3 = pattern3.matcher("Java 7 Java 7");
        Matcher matcher4 = pattern3.matcher("Java 9");

        while (matcher3.find()) {
            System.out.println(matcher3.group()); //Java Java
        }

        if (!matcher4.find()) System.out.println("Coincidences not found"); //Coincidences not found

        Pattern pattern4 = Pattern.compile("Java (?!7|8)");
        Matcher matcher5 = pattern4.matcher("Java 7 Java 7");
        Matcher matcher6 = pattern4.matcher("Java 9");

        if (!matcher5.find()) System.out.println("Coincidences not found"); //Coincidences not found

        while (matcher6.find()) {
            System.out.println(matcher6.group()); //Java
        }


    }
}