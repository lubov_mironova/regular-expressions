package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class POSIXCharacterClasses {
    public static void main(String[] args) {

        int counter = 0;
        String string = "abcd1234";
        Pattern pattern = Pattern.compile("\\p{Digit}");
        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            counter++;
            System.out.println("Совпадение найдено '" +
                    string.substring(matcher.start(), matcher.end()) +
                    "' начиная с индекса " + matcher.start() +
                    " и заканчивая индесом " + matcher.end());
        }

        System.out.println("Совпадений найдено: " + counter); // 4

        int counter1 = 0;
        String s = "!@#$%^&*abcd1234";
        Pattern pattern1 = Pattern.compile("\\p{Punct}");
        Matcher matcher1 = pattern1.matcher(s);

        while (matcher1.find()) {
            counter1++;
            System.out.println("Совпадение найдено '" +
                    string.substring(matcher1.start(), matcher1.end()) +
                    "' начиная с индекса " + matcher1.start() +
                    " и заканчивая индесом " + matcher1.end());
        }

        System.out.println("Совпадений найдено: " + counter1); // 8
    }
}
