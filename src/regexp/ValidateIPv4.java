package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateIPv4 {
    public static void main(String[] args) {

        int counter = 0;
        String iPv4 = "0.0.0.0";
        Pattern pattern = Pattern.compile("^(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))$");
        Matcher matcher = pattern.matcher(iPv4);

        while (matcher.find()) {
            counter++;
            System.out.println("Совпадение найдено '" +
                    iPv4.substring(matcher.start(), matcher.end()) +
                    "' начиная с индекса " + matcher.start() +
                    " и заканчивая индесом " + matcher.end());
        }

        System.out.println("Совпадений найдено: " + counter); // 1
    }
}