package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateIPv6 {
    public static void main(String[] args) {

        int counter = 0;
        String iPv6 = "77:ffcc::192.168.0.1";
        Pattern pattern = Pattern.compile(
                "^(" +
                        "(\\p{XDigit}{1,4}:){7}\\p{XDigit}{1,4}|" +
                        "(\\p{XDigit}{1,4}:){1,7}:|" +
                        "(\\p{XDigit}{1,4}:){1,6}(:(\\p{XDigit}{1,4}))|" +
                        "(\\p{XDigit}{1,4}:){1,5}(:(\\p{XDigit}{1,4})){1,2}|" +
                        "(\\p{XDigit}{1,4}:){1,4}(:(\\p{XDigit}{1,4})){1,3}|" +
                        "(\\p{XDigit}{1,4}:){1,3}(:(\\p{XDigit}{1,4})){1,4}|" +
                        "(\\p{XDigit}{1,4}:){1,2}(:(\\p{XDigit}{1,4})){1,5}|" +
                        "(\\p{XDigit}{1,4}:)(:(\\p{XDigit}{1,4})){1,6}|" +
                        "(:((:(\\p{XDigit}{1,4})){1,7})|:)|" +
                        "::(ffff(:0{1,4})?:)?((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])|" +
                        "((\\p{XDigit}{1,4}):){1,4}:((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])|" +
                        ")$");

        Matcher matcher = pattern.matcher(iPv6);

        while (matcher.find()) {
            counter++;
            System.out.println("Совпадение найдено '" +
                    iPv6.substring(matcher.start(), matcher.end()) +
                    "' начиная с индекса " + matcher.start() +
                    " и заканчивая индесом " + matcher.end());
        }

        System.out.println("Совпадений найдено: " + counter); // 1
    }
}