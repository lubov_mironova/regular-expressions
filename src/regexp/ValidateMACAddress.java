package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateMACAddress {
    public static void main(String[] args) {

        int counter = 0;
        int counter1 = 0;
        String mAC = "77:a3-d2:01-ff:63";
        Pattern pattern = Pattern.compile("(((\\p{XDigit}{2})[:-]){5}\\p{XDigit}{2})$");
        Pattern pattern1 = Pattern.compile("((\\p{XDigit}{2}([:-]|$)){6})$");
        Matcher matcher = pattern.matcher(mAC);
        Matcher matcher1 = pattern1.matcher(mAC);

        while (matcher.find()) {
            counter++;
            System.out.println("Совпадение найдено '" +
                    mAC.substring(matcher.start(), matcher.end()) +
                    "' начиная с индекса " + matcher.start() +
                    " и заканчивая индесом " + matcher.end());
        }

        System.out.println("Совпадений найдено: " + counter); // 1

        while (matcher1.find()) {
            counter1++;
            System.out.println("Совпадение найдено '" +
                    mAC.substring(matcher1.start(), matcher1.end()) +
                    "' начиная с индекса " + matcher1.start() +
                    " и заканчивая индесом " + matcher1.end());
        }

        System.out.println("Совпадений найдено: " + counter1); // 1
    }
}
