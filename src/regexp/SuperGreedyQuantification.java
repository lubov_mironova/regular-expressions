package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SuperGreedyQuantification {
    public static void main(String[] args) {

        int counter = 0;
        String string = "192.168.1.197";
        Pattern pattern = Pattern.compile(".*+19");
        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            counter++;
            System.out.println("Совпадение найдено '" +
                    string.substring(matcher.start(), matcher.end()) +
                    "' начиная с индекса " + matcher.start() +
                    " и заканчивая индесом " + matcher.end());
        }

        System.out.println("Совпадений найдено: " + counter); // 0
    }
}
