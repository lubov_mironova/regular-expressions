package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationOfE_mail {
    public static void main(String[] args) {

        int counter = 0;
        String email = "example@gmail.com";
        Pattern pattern = Pattern.compile("^((\\w|[-+])+(\\.[\\w-]+)*@[\\w-]+((\\.[\\d\\p{Alpha}]+)*(\\.\\p{Alpha}{2,})*)*)$");
        Matcher matcher = pattern.matcher(email);

        while (matcher.find()) {
            counter++;
            System.out.println("Совпадение найдено '" +
                    email.substring(matcher.start(), matcher.end()) +
                    "' начиная с индекса " + matcher.start() +
                    " и заканчивая индесом " + matcher.end());
        }

        System.out.println("Совпадений найдено: " + counter); // 1
    }
}
