package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LiteralsAndMetacharacters {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile("^[a-e]");

        Matcher matcher1 = pattern1.matcher("a b c d e f g h");
        System.out.println(matcher1.find()); // true

        Matcher matcher2 = pattern1.matcher("f g h a b c");
        System.out.println(matcher1.find()); // false

        Pattern pattern3 = Pattern.compile("[a-e]$");

        Matcher matcher3 = pattern3.matcher("a b c d e f g h");
        System.out.println(matcher3.find()); // false

        Matcher matcher4 = pattern3.matcher("f g h a b c");
        System.out.println(matcher4.find()); // true

        Pattern pattern = Pattern.compile(".[0-9]");

        Matcher matcher5 = pattern.matcher("a b c d e f g h");
        System.out.println(matcher5.find()); // false

        Matcher matcher6 = pattern.matcher("f g h a b c");
        System.out.println(matcher6.find()); // false

        Matcher matcher7 = pattern.matcher("1 2 3");
        System.out.println(matcher7.find()); // true

        Pattern pattern4 = Pattern.compile(".[0-9]|.[a-c]");

        Matcher matcher8 = pattern4.matcher("a b c d e f g h");
        System.out.println(matcher8.find()); // true

        Matcher matcher9 = pattern4.matcher("f g h a b c");
        System.out.println(matcher9.find()); // true

        Matcher matcher10 = pattern4.matcher("1 2 3");
        System.out.println(matcher10.find()); // true

        Pattern pattern5 = Pattern.compile("\\d");

        Matcher matcher11 = pattern5.matcher("a b c d e f g h");
        System.out.println(matcher11.find()); // false

        Matcher matcher12 = pattern5.matcher("f g h a b c");
        System.out.println(matcher12.find()); // false

        Matcher matcher13 = pattern5.matcher("1 2 3");
        System.out.println(matcher13.find()); // true

        Pattern pattern6 = Pattern.compile("\\D");

        Matcher matcher14 = pattern6.matcher("a b c d e f g h");
        System.out.println(matcher14.find()); // true

        Matcher matcher15 = pattern6.matcher("f g h a b c");
        System.out.println(matcher15.find()); // true

        Matcher matcher16 = pattern6.matcher("2");
        System.out.println(matcher16.find()); // false

        Pattern pattern7 = Pattern.compile("\\s");

        Matcher matcher17 = pattern7.matcher("a b c d e f g h");
        System.out.println(matcher17.find()); // true

        Matcher matcher18 = pattern7.matcher("f g h a b c");
        System.out.println(matcher18.find()); // true

        Matcher matcher19 = pattern7.matcher("2");
        System.out.println(matcher19.find()); // false

        Pattern pattern8 = Pattern.compile("\\S");

        Matcher matcher20 = pattern8.matcher("a b c d e f g h");
        System.out.println(matcher20.find()); // true

        Matcher matcher21 = pattern8.matcher("f g h a b c");
        System.out.println(matcher21.find()); // true

        Matcher matcher22 = pattern8.matcher("2");
        System.out.println(matcher22.find()); // true
    }
}
