package regexp;

import java.util.regex.Pattern;

public class Quantifier {
    public static void main(String[] args) {
        System.out.println(Pattern.compile("10{2}").matcher("10").find()); // false
        System.out.println(Pattern.compile("10{2}").matcher("100").find()); // true

        System.out.println(Pattern.compile("10{1,3}").matcher("100").find()); // true
        System.out.println(Pattern.compile("10{1,3}").matcher("1").find()); // false

        System.out.println(Pattern.compile("10{2,}").matcher("1000").find()); // true
        System.out.println(Pattern.compile("10{2}").matcher("10").find()); // false

        System.out.println(Pattern.compile("10?").matcher("10").find()); // true

        System.out.println(Pattern.compile("10*").matcher("1").find()); // true

        System.out.println(Pattern.compile("10+").matcher("1").find()); // false
        System.out.println(Pattern.compile("10+").matcher("10").find()); // true
    }
}